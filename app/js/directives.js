'use strict';

/* Directives */


angular.module('QnA.directives', []).
    directive('integer',function () {
        return {
            require:'ngModel',
            link:function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function (viewValue) {
                    if (/^\-?\d*$/.test(viewValue)) {
                        ctrl.$setValidity('integer', true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity('integer', false);
                        return undefined;
                    }
                });
            }
        };
    }).
    directive('buttonsRadio', function () {
        return {
            scope:{ model:'=', options:'='},
            controller:function ($scope) {
                $scope.activate = function (option) {
                    $scope.model = option;
                };
            },
            template:"<button type='button' class='btn' " +
                "ng-class='{active: option == model}'" +
                "ng-repeat='option in options' " +
                "ng-click='activate(option)'>{{option}} " +
                "</button>"
        };
    }).
    directive('zippy', function(){
        return {
            restrict: 'C',
            replace: true,
            transclude: true,
            scope: { title:'@zippyTitle' },
            template: '<div>' +
                '<div class="title">{{title}}</div>' +
                '<div class="body" ng-transclude></div>' +
                '</div>',
            link: function(scope, element, attrs) {
                var title = angular.element(element.children()[0]),
                    opened = true;

                title.bind('click', toggle);
                function toggle() {
                    opened = !opened;
                    element.removeClass(opened ? 'closed' : 'opened');
                    element.addClass(opened ? 'opened' : 'closed');
                }
                toggle();
            }
        }
    });