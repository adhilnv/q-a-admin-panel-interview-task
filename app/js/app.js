'use strict';

// Declare app level module which depends on filters, and services and routes
angular.module('QnA', ['QnA.filters', 'QnA.services', 'QnA.directives']).
    config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/qna', {templateUrl:'partials/new.html', controller:NewCtrl})
    $routeProvider.when('/qna/details', {templateUrl:'partials/details.html', controller:DetailsCtrl})
    $routeProvider.when('/qna/details/edit', {templateUrl:'partials/detailsEdit.html', controller:DetailsEditCtrl})
    $routeProvider.when('/qna/templates/edit', {templateUrl:'partials/templates.html', controller:TemplatesCtrl });
    $routeProvider.when('/qna/questions', {templateUrl:'partials/questions.html', controller:QuestionsCtrl });
    $routeProvider.when('/qna/questions/dummy', {templateUrl:'partials/questionsScaffolding.html', controller:QuestionsScaffoldingCtrl });
    $routeProvider.otherwise({redirectTo:'/404', templateUrl:'partials/404.html'});
}]).
    run(function ($rootScope, $location, Details) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            var details = Details.get();
            if (next.templateUrl == "partials/new.html") {
                if (details.title) {
                    $location.path('/qna/details')
                }
            }
            else if (next.templateUrl == "partials/details.html") {
                if (details.email) {
                    $location.path('/qna/questions')
                }
                else if(!details.title) {
                    $location.path('/qna')
                }
            }
            else if (next.templateUrl == "partials/questions.html") {
                if (!details.email) {
                    if (!details.title) {
                        $location.path('/qna')
                    }
                    else {
                        $location.path('/qna/details')
                    }
                }
            }
        });
    });

//Controllers

var NewCtrl = function ($scope, $location, Details) {
    $scope.next = function (title, introduction) {
        Details.update({title:title, introduction:introduction});
        $location.path('/qna/details')
    };
}

var DetailsCtrl = function ($scope, $location, Details) {
    $scope.details = Details.get();

    $scope.update = function (email, thankyou) {
        Details.update({email:email, thankyou:thankyou});
        $location.path('/qna/questions');
    };
}

var DetailsEditCtrl = function ($scope, $location, Details) {
    var details = Details.get();

    $scope.title = details.title;
    $scope.introduction = details.introduction;
    $scope.email = details.email;
    $scope.thankyou = details.thankyou;

    $scope.update = function (title, introduction, email, thankyou) {
        Details.update({title:title, introduction:introduction, email:email, thankyou:thankyou});
        $location.path('/qna');
    };
}

var TemplatesCtrl = function ($scope, $location, Templates) {

    var templates = Templates.get();
    $scope.data = {};
    $scope.data.adminSubject = templates.adminSubject;
    $scope.data.adminBody = templates.adminBody;
    $scope.data.userSubject = templates.userSubject;
    $scope.data.userBody = templates.userBody;
    $scope.data.userPrivateSubject = templates.userPrivateSubject;
    $scope.data.userPrivateBody = templates.userPrivateBody;
    $scope.data.anonymousSubject = templates.anonymousSubject;
    $scope.data.anonymousBody = templates.anonymousBody;

    $scope.update = function (adminSubject, adminBody, userSubject, userBody, userPrivateSubject, userPrivateBody, anonymousSubject, anonymousBody) {
        Templates.update({adminSubject:adminSubject, adminBody:adminBody, userSubject:userSubject, userBody:userBody, userPrivateSubject:userPrivateSubject, userPrivateBody:userPrivateBody, anonymousSubject:anonymousSubject, anonymousBody:anonymousBody});
        $location.path('/qna');
    };
}

var QuestionsCtrl = function ($scope, $location, Details, Questions) {
    $scope.details = Details.get();
    $scope.questions = Questions.get();

    $scope.setStatus = function (status) {
        Details.update({status:status});
        $location.path('/qna');
    };
   $scope.moveTo = function (id,status) {
            console.log(status)
            Questions.update({id:id,status:status})

    };
    $scope.filterOptions = ["New", "Publicly Answered", "Privately Answered", "Archived", "Junk"];
    $scope.filterCriteria = {status:"New"};
}
var QuestionsScaffoldingCtrl = function ($scope, $location, Questions) {
    $scope.add = function (username,question) {
        Questions.save({username: username, question: question, status: "New" });
        $location.path('/qna/questions');
    };
}

var MenuCtrl = function ($scope, $location) {
    $scope.location = $location;
}
