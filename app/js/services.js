'use strict';

/* Services */

angular.module('QnA.services', []).
    value('localStorage', window.localStorage).
    factory('dbUtilities', function ($rootScope) {
        var db={};
        db.createPersistentProperty = function(localName, storageName, Type) {
            var json = localStorage[storageName];

            db[localName] = json ? JSON.parse(json) : new Type;

            $rootScope.$watch(
                function () {
                    return db[localName];
                },
                function (value) {
                    if (value) {
                        localStorage[storageName] = JSON.stringify(value);
                    }
                },
                true);

        }

        return db;
    }).
    service('Details',function (localStorage, dbUtilities) {
        var self = this;

        self.update = function (details) {
            if (details.title) {
                dbUtilities.details.title = details.title;
            }
            if (details.introduction) {
                dbUtilities.details.introduction = details.introduction;
            }
            if (details.email) {
                dbUtilities.details.email = details.email;
            }
            if (details.thankyou) {
                dbUtilities.details.thankyou = details.thankyou;
            }
            if(details.status) {
                dbUtilities.details.status = details.status;
            }
            else if(!details.status) {
                dbUtilities.details.status = "Draft";
            }
        };

        self.get = function() {
            return dbUtilities.details;
        };

        dbUtilities.createPersistentProperty('details', 'qnaDetails', Object);

    }).
    service('Templates',function (localStorage, dbUtilities) {
        var self = this;

        self.update = function (templates) {
            console.log(templates);
                dbUtilities.templates = templates;
        };

        self.get = function() {
            return dbUtilities.templates;
        };

        dbUtilities.createPersistentProperty('templates', 'qnaTemplates', Object);

        if(!dbUtilities.templates.adminSubject) {
            dbUtilities.templates = {
                adminSubject: "A new question has been added to {{title}}",
                adminBody: "" +
                    "Hi there,\n\n" +
                    "Just a quick heads up to let you know that a new question has been asked at PortalName." +
                    "The question that was asked is: {{Question}}\n\n" +
                    "Regards",
                userSubject: "",
                userBody: "",
                userPrivateSubject: "",
                userPrivateBody: "",
                anonymousSubject: "",
                anonymousBody: ""
            };
        }

    }).
    service('Questions',function (localStorage, $rootScope,dbUtilities) {

        var self = this;

        self.save = function (question) {
            if (!question.hasOwnProperty('id')) {
                var highest = 1;
                for (var i = 0; i < dbUtilities.questions.length; i++) {
                    if (dbUtilities.questions[i].id > highest) {
                        highest = dbUtilities.questions[i].id;
                    }
                }
                question.id = ++highest;
            }
            dbUtilities.questions.push(question);
            return question.id;
        };

        self.update = function (question) {
            for (var i = 0; i < dbUtilities.questions.length; i++) {
                if (dbUtilities.questions[i].id == question.id) {
                    if (question.status) {
                        dbUtilities.questions[i].status = question.status;
                    }
                    if (question.privateAnswer) {
                        if(dbUtilities.questions[i].hasOwnProperty(privateAnswers)) {
                            dbUtilities.questions[i].privateAnswers.push(question.privateAnswer);
                        }
                        else {
                            dbUtilities.questions[i].privateAnswers = [];
                            dbUtilities.questions[i].privateAnswers.push(question.privateAnswer);
                        }
                    }
                    if (question.publicAnswer) {
                        if(dbUtilities.questions[i].hasOwnProperty(publicAnswers)) {
                            dbUtilities.questions[i].publicAnswers.push(question.publicAnswer);
                        }
                        else {
                            dbUtilities.questions[i].publicAnswers = [];
                            dbUtilities.questions[i].publicAnswers.push(question.publicAnswer);
                        }
                    }

                }
            }
            return question.id;
        };

        self.get = function () {
            return dbUtilities.questions;
        };

        self.remove = function (id) {
            for (var i = 0; i < dbUtilities.questions.length; i++) {
                if (dbUtilities.questions[i].id == id) {
                    dbUtilities.questions.splice(i, 1);
                }
            }
        };
        
        dbUtilities.createPersistentProperty('questions', 'qnaQuestions', Array);

    });

